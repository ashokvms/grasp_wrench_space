# -*- coding: utf-8 -*-

import numpy
from grasp_wrench_space import grasp_wrench_space
import sys

if len(sys.argv) != 3:
    print 'Please specify two arguments: (int) Example number (1 to 4), (string) Union-GWS (union) or Minkowski-GWS (minkowski)'
    sys.exit()

if int(sys.argv[1]) < 0 or int(sys.argv[1]) > 4 or (sys.argv[2] != 'union' and sys.argv[2] != 'minkowski'):
    print 'Wrong argument values passed'
    print 'Please specify two arguments: (int) Example number (1 to 4), (string) Union-GWS (union) or Minkowski-GWS (minkowski)'
    sys.exit()

log_level = 10
create_plots = True
contact_dim = '2d'

if int(sys.argv[1]) == 1:
    object_com = [0, 0]
    frictions = [0.5,0.5,0.5]
    contacts = [[13,3,-0.057,-0.083],
                [-13,3,0.057,-0.083],
                [0,-6,0,1]]
    normal_forces = [2.3,2.3,4.0]
    friction_cone_discretization_factors = [2,2,2]

elif int(sys.argv[1]) == 2: #5 contacts on one side and one contact on the other side
    d = 1.5
    f = 6.0
    x = 2.0
    object_com = [0, 0]
    frictions = [0.5,0.5,0.5,0.5,0.5,0.5]
    contacts = [[-x,d,1,0],
                [-x,0,1,0],
                [-x,-d,1,0],
                [x,0,-1,0],
                [-x,2*d,1,0],
                [-x,-2*d,1,0]]
    normal_forces = [f/5,f/5,f/5,f,f/5,f/5]
    friction_cone_discretization_factors = [3,3,3,3,3,3]

elif int(sys.argv[1]) == 3: #3 contacts on one side and one contact on the other side
    d = 1.5
    f = 6.0
    x = 2.0
    object_com = [0, 0]
    frictions = [0.5,0.5,0.5,0.5]
    contacts = [[-x,d,1,0],
                [-x,0,1,0],
                [-x,-d,1,0],
                [x,0,-1,0]]
    normal_forces = [f/3,f/3,f/3,f]
    friction_cone_discretization_factors = [2,2,2,2]

elif int(sys.argv[1]) == 4: #2 contacts on one side and one contact on the other side, non force closure example
    d = 1.5
    f = 6.0
    x = 2.0
    object_com = [0, 0]
    frictions = [0.5,0.5,0.5,0.5]
    contacts = [[-x,d/2,1,0],
                [-x,0,1,0],
                [x,-d-d/2,-1,0]]
    normal_forces = [f/3,f/3,f/3]
    friction_cone_discretization_factors = [2,2,2]

gws = grasp_wrench_space.GraspWrenchSpace(object_com = object_com, # center of mass of the object [x_position, y_position]
                                          contact_frictions = frictions, # list containing friction at each contact
                                          contact_points = contacts, # list of contacts, each contact list contains [x_position, y_position, x_normal, y_normal]
                                          contact_normal_forces = normal_forces, # list containing normal force at each contact
                                          friction_cone_discretization_factors = friction_cone_discretization_factors, # list containing friction cone discretization factor at each contact
                                          contact_dimension = contact_dim, # contact dimension 2D or 3D
                                          show_plots = False, # True to show plots, but will block execution
                                          logger_level = log_level) # python logging level

if sys.argv[2] == 'union':
    grasp_quality, time = gws.calculate_union_gws()
if sys.argv[2] == 'minkowski':
    grasp_quality, time = gws.calculate_minkowski_gws()

print 'largest minimum resisted wrench: ', grasp_quality
if grasp_quality > 0:
    print 'force closure: True'
else:
    print 'force closure: False'

if create_plots == True:
    gws.create_plots() # show the plots - blocks until window is closed
