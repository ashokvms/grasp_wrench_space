# -*- coding: utf-8 -*-

import numpy
from grasp_wrench_space import grasp_wrench_space
import sys

if len(sys.argv) != 3:
    print 'Please specify two arguments: (int) Example number (1 to 4), (string) Union-GWS (union) or Minkowski-GWS (minkowski)'
    sys.exit()

if int(sys.argv[1]) < 0 or int(sys.argv[1]) > 4 or (sys.argv[2] != 'union' and sys.argv[2] != 'minkowski'):
    print 'Wrong argument values passed'
    print 'Please specify two arguments: (int) Example number (1 to 4), (string) Union-GWS (union) or Minkowski-GWS (minkowski)'
    sys.exit()

log_level = 10
create_plots = True
contact_dim = '3d'

if int(sys.argv[1]) == 1: #5 contacts on one side and one contact on the other side
    d = 1.5
    f = 6.0
    x = 2.0
    object_com = [0, 0, 0]
    frictions = [0.5,0.5,0.5,0.5,0.5,0.5]
    contacts = [[-x,0,d,1,0,0],
                [-x,0,0,1,0,0],
                [-x,0,-d,1,0,0],
                [x,0,0,-1,0,0],
                [-x,0,2*d,1,0,0],
                [-x,0,-2*d,1,0,0]]

    normal_forces = [f/5,f/5,f/5,f,f/5,f/5]
    friction_cone_discretization_factors = [3,3,3,3,3,3]

elif int(sys.argv[1]) == 2: #3 contacts on one side and one contact on the other side
    d = 1.5
    f = 6.0
    x = 2.0
    object_com = [0, 0, 0]
    frictions = [0.5,0.5,0.5,0.5]
    contacts = [[-x,0,d,1,0,0],
                [-x,0,0,1,0,0],
                [-x,0,-d,1,0,0],
                [x,0,0,-1,0,0]]
    normal_forces = [f/3,f/3,f/3,f]
    friction_cone_discretization_factors = [4,4,4,4]

elif int(sys.argv[1]) == 3: #2 contacts on one side and one contact on the other side, non force closure example
    d = 1.5
    f = 6.0
    x = 2.0
    object_com = [0, 0, 0]
    frictions = [0.5,0.5,0.5]
    contacts = [[-x,0,0,1,0,0],
                [-x,0,-d,1,0,0],
                [x,0,d+d/2,-1,0,0]]
    normal_forces = [f/3,f/3,f/3]
    friction_cone_discretization_factors = [4,4,4]

elif int(sys.argv[1]) == 4:

    object_com = [0, 0, 0]
    frictions = [0.5,0.5,0.5,0.5,0.5]
    contacts = [[-0.04275289922952652, 0.0005646370118483901, 0.22441600263118744, -0.9403190016746521, -0.3392289876937866,0.026886099949479103],
                [-0.030062099918723106,0.03821209818124771,0.23833100497722626,-0.6411030292510986,0.7499759793281555,0.1628579944372177],
                [0.030443299561738968,0.02221529930830002,0.19432200491428375,0.9299020171165466,0.3666580021381378,0.02906000055372715],
                [-0.006476739887148142,-0.02706810086965561,0.2183780074119568,-0.0,-1.0,-0.0],
                [0.028938399627804756,-0.0031032399274408817,0.22035899758338928,0.9309200048446655,-0.3553209900856018,0.08446569740772247]]
    normal_forces = [3.0,3.0,3.0,3.0,3.0]
    friction_cone_discretization_factors = [5,5,5,5,5]


gws = grasp_wrench_space.GraspWrenchSpace(object_com = object_com, # center of mass of the object [x_position, y_position, z_position]
                                          contact_frictions = frictions, # list containing friction at each contact
                                          contact_points = contacts, # list of contacts, each contact list contains [x_position, y_position, z_position, x_normal, y_normal, z_normal]
                                          contact_normal_forces = normal_forces, # list containing normal force at each contact
                                          friction_cone_discretization_factors = friction_cone_discretization_factors, # list containing friction cone discretization factor at each contact
                                          contact_dimension = contact_dim, # contact dimension 2D or 3D
                                          show_plots = False, # True to show plots, but will block execution
                                          logger_level = log_level) # python logging level

if sys.argv[2] == 'union':
    grasp_quality, time = gws.calculate_union_gws()
if sys.argv[2] == 'minkowski':
    grasp_quality, time = gws.calculate_minkowski_gws()

print 'largest minimum resisted wrench: ', grasp_quality
if grasp_quality > 0:
    print 'force closure: True'
else:
    print 'force closure: False'

if create_plots == True:
    gws.create_plots() # show the plots - blocks until window is closed
