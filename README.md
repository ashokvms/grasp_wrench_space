# grasp wrench space
Calculates the grasp wrench space for 2D or 3D contacts and returns the largest minimum resisted wrench (for non force closure grasps returns 0.0)

# Usage

Update the python path.

```
export PYTHONPATH=workspace_dir/grasp_wrench_space:$PYTHONPATH
```

See scripts/example_2d.py and scripts/example_3d.py for usage examples. To run the example:

```
python workspace_dir/grasp_wrench_space/scripts/example_3d.py 1 union
```

To use in your code:

```
gws = grasp_wrench_space.GraspWrenchSpace(object_com = object_com, # center of mass of the object [x_position, y_position, z_position]
                                          contact_frictions = frictions, # list containing friction at each contact
                                          contact_points = contacts, # list of contacts, each contact list contains [x_position, y_position, z_position, x_normal, y_normal, z_normal]
                                          contact_normal_forces = normal_forces, # list containing normal force at each contact
                                          friction_cone_discretization_factors = friction_cone_discretization_factors, # list containing friction cone discretization factor at each contact
                                          contact_dimension = contact_dim, # contact dimension 2D or 3D
                                          show_plots = False, # True to show plots, but will block execution
                                          logger_level = log_level) # python logging level

grasp_quality_1, time_1 = gws.calculate_union_gws() # faster, uses union of wrenches
grasp_quality_2, time_2 = gws.calculate_minkowski_gws() # slower, uses minkowski sum of wrenches
```

For 2D case, the plot shows the 3D grasp wrench space and the closest facet is highlighted. For 3D case, since the grasp wrench space is in 6D, two 3D representations are made separately for grasp force and torque. Additionaly, the object primitive forces and torques are also shown.
