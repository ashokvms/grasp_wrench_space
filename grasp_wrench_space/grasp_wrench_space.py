#!/usr/bin/env python
"""
This module contains functions to calculate convex hull of the grasp wrenches (union or minkowski sum) exerted through
the contacts. The convex hull is used to find force closure and largest minimum resisted wrench.
"""

__author__ = 'ashok'

import numpy
import scipy.spatial as sp_spatial
import logging
import itertools
import time

import matplotlib as mpl
mpl.use('GTKAgg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits import mplot3d

class GraspWrenchSpace(object):
    
    def __init__(self, contact_dimension = '3d', object_com = None, contact_points = None, contact_frictions = None,
                 friction_cone_discretization_factors = None, contact_normal_forces = None, show_plots = False, logger_level = 10):
        self.contact_dimension = contact_dimension
        self.object_com = object_com
        self.contact_points = contact_points
        self.contact_frictions = contact_frictions
        self.friction_cone_discretization_factors = friction_cone_discretization_factors
        self.contact_normal_forces = contact_normal_forces
        self.show_plots = show_plots 

        self.configure_logging(logger_level)        
        self.initialize_variables()
        
        self.logger.info('Grasp Wrench Space Module Initialized')
        
    def __del__(self):
        for handler in self.logger.handlers:
            self.logger.removeHandler(handler)
        
    def initialize_variables(self):
        self.cone_wise_primitive_forces = []
        self.contact_wise_object_primitive_forces = []
        self.contact_wise_object_primitive_wrenches = []
        self.wrenches = []
        self.gws = None
        self.is_force_closure_grasp = False
        self.largest_minimum_resisted_wrench = 0.0
        self.largest_minimum_resisted_wrench_plane = None
        self.largest_minimum_resisted_wrench_simplex = None
        
    def configure_logging(self, logger_level):
        self.logger = logging.getLogger('GWS')
        self.logger.setLevel(logger_level)
        self.logger.propagate = False
        console_handle = logging.StreamHandler()
        console_handle.setFormatter(logging.Formatter('%(name)s - %(levelname)s: %(message)s'))
        self.logger.addHandler(console_handle)
    
    def calculate_union_gws(self):
        self.logger.info('==================================Union GWS==================================')
        self.initialize_variables()
        start_time = time.clock()
        self.construct_gws("union")
        end_time = time.clock()-start_time
        self.logger.info('Total Time Taken is %f seconds', end_time)
        if self.show_plots:
            self.logger.info('Plots are being created')
            self.create_plots()
        self.logger.info('*****************************************************************************')
        return self.largest_minimum_resisted_wrench, end_time

    def calculate_minkowski_gws(self):
        self.logger.info('================================Minkowski GWS================================')        
        self.initialize_variables()
        start_time = time.clock()
        self.construct_gws("minkowski")
        end_time = time.clock()-start_time
        self.logger.info('Total Time Taken is %f seconds', end_time)
        if self.show_plots:
            self.logger.info('Plots are being created')
            self.create_plots()
        self.logger.info('*****************************************************************************')
        return self.largest_minimum_resisted_wrench, end_time

    def construct_gws(self,gws_type):
        self.construct_wrench_set(gws_type)        
        self.create_convex_hull_of_wrenches()
        self.calculate_fc_with_minimum_resisted_wrench()
        
    def construct_wrench_set(self,gws_type):
        for i in range(len(self.contact_points)):
            self.cone_wise_primitive_forces.append(list(self.calculate_cone_primitive_forces(self.friction_cone_discretization_factors[i], self.contact_frictions[i], self.contact_normal_forces[i])))
            self.contact_wise_object_primitive_forces.append(list(self.calculate_object_primitive_forces(self.contact_points[i], self.cone_wise_primitive_forces[i])))
            self.contact_wise_object_primitive_wrenches.append(list(self.calculate_object_primitive_wrenches(self.contact_points[i], self.contact_wise_object_primitive_forces[i])))
        
        if gws_type == "minkowski": 
            self.create_possible_combination_of_wrenches()
        elif gws_type == "union":
            self.create_union_of_wrenches()
            
        return self.wrenches

    def calculate_cone_primitive_forces(self, cone_discritization_factor, friction, normal_force):
        if self.contact_dimension == '3d':
            friction_cone_angles = numpy.linspace(0,2*numpy.pi,cone_discritization_factor+1)
            friction_cone_angles = friction_cone_angles[0:len(friction_cone_angles)-1]
            cone_primitive_forces = normal_force * numpy.c_[friction*numpy.cos(friction_cone_angles), friction*numpy.sin(friction_cone_angles), numpy.ones(len(friction_cone_angles))]
            if friction == 0:
                cone_primitive_forces = numpy.array([cone_primitive_forces[0]])
        elif self.contact_dimension == '2d':
            direction = numpy.array([1,-1])
            cone_primitive_forces = normal_force * numpy.c_[friction*direction, numpy.ones(len(direction))]
            if friction == 0:
                cone_primitive_forces = numpy.array([cone_primitive_forces[0]])
        return cone_primitive_forces
        
    def calculate_object_primitive_forces(self, contact, cone_primitive_forces):
        if self.contact_dimension == '3d':
            #rot_matrix = self.rotation_matrix_from_quat(self.quat_rotate_direction(contact[3:6],numpy.array((0,0,1))))
            # probably the above is wrong - below is consistent with theo's implementation and looks correct with initial check
            rot_matrix = self.rotation_matrix_from_quat(self.quat_rotate_direction(numpy.array((0,0,1)),contact[3:6]))
            object_primitive_forces = numpy.dot(cone_primitive_forces,numpy.transpose(rot_matrix))
        elif self.contact_dimension == '2d':
            #Reference: http://www.euclideanspace.com/maths/algebra/vectors/angleBetween/
            #Reference: http://www.euclideanspace.com/maths/geometry/trig/inverse/index.htm
            theta = numpy.arctan2(contact[2],contact[3])*-1
            #Reference: http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
            rot_matrix = numpy.array([[numpy.cos(theta), -numpy.sin(theta)], [numpy.sin(theta), numpy.cos(theta)]])         
            object_primitive_forces = numpy.dot(cone_primitive_forces,numpy.transpose(rot_matrix))
        return object_primitive_forces
        
    def calculate_object_primitive_wrenches(self, contact, object_primitive_forces):
        if self.contact_dimension == '3d':
            contact_position = numpy.array(contact[0:3])
        elif self.contact_dimension == '2d':
            contact_position = numpy.array(contact[0:2])
        contact_distance_to_obj_com = contact_position-self.object_com
        object_primitive_wrenches = numpy.c_[object_primitive_forces, numpy.cross(numpy.tile(contact_distance_to_obj_com,(len(object_primitive_forces),1)),object_primitive_forces)]
        return object_primitive_wrenches
        
    def create_union_of_wrenches(self):
        union_of_wrenches = [item for sublist in self.contact_wise_object_primitive_wrenches for item in sublist]
        self.wrenches = numpy.asarray(union_of_wrenches)
        
    def create_possible_combination_of_wrenches(self):
        
        def get_minkowski_sum(set1, set2):
            minkowski_sum = []        
            for vector1 in set1:
                for vector2 in set2:
                    minkowski_sum.append(vector1+vector2)
            return minkowski_sum

        def get_minkowski_sum_of_wrenches(contact_wise_wrenches):
            minkowski_sum = []
            for i in range (len(contact_wise_wrenches)-1):
                if (i==0):
                    minkowski_sum = get_minkowski_sum(contact_wise_wrenches[i], contact_wise_wrenches[i+1])
                else:
                    minkowski_sum = get_minkowski_sum(minkowski_sum, contact_wise_wrenches[i+1])
            return numpy.asarray(minkowski_sum)        
        
        minkowski_sum = []
        for i in range (len(self.contact_wise_object_primitive_wrenches)):
            possible_combinations = list(itertools.combinations(self.contact_wise_object_primitive_wrenches,i+1))
            if len(possible_combinations[0]) == 1:
                minkowski_sum.append(list(itertools.chain.from_iterable(list(itertools.chain.from_iterable(possible_combinations)))))
            else:
                for j in range (len(possible_combinations)):
                    minkowski_sum.append(get_minkowski_sum_of_wrenches(possible_combinations[j]))
        self.wrenches = numpy.asarray(list(itertools.chain.from_iterable(minkowski_sum)))
    
    def create_convex_hull_of_wrenches(self):
        start_time = time.clock()
        self.gws = sp_spatial.ConvexHull(self.wrenches)
        self.logger.info('Time taken for GWS creation is %f seconds', time.clock()-start_time)
        
    def get_centroid_of_hull(self):
        return numpy.array([numpy.mean(self.gws.points[self.gws.vertices,i]) for i in range(self.gws.points.shape[1])])
        
    def get_point_to_plane_distance(self, plane, point):
        #Reference: http://mathworld.wolfram.com/Point-PlaneDistance.html
        num = 0
        den = 0
        for i in range(point.shape[0]):
            num = num + point[i]*plane[i]
            den = den + numpy.power(plane[i],2)
        num = num + plane[-1]
        den = numpy.sqrt(den)
        return num/den
    
    def calculate_fc_with_minimum_resisted_wrench(self):
        start_time = time.clock()
        centroid = self.get_centroid_of_hull()
        distance = []
        origin = numpy.zeros(self.gws.equations.shape[1]-1)
        for plane in self.gws.equations:
            distance_to_origin = self.get_point_to_plane_distance(plane,origin)
            distance.append(numpy.fabs(distance_to_origin))
            distance_to_centroid = self.get_point_to_plane_distance(plane,centroid)
            if((distance_to_origin>0.0 and distance_to_centroid<0.0) or (distance_to_origin<0.0 and distance_to_centroid>0.0)):
                self.largest_minimum_resisted_wrench = 0.0
                self.largest_minimum_resisted_wrench_plane = None
                self.largest_minimum_resisted_wrench_simplex = None                
                self.is_force_closure_grasp = False
                self.logger.info('Time taken for FC with minimum resisted wrench is %f seconds', time.clock()-start_time)
                self.logger.info('Force closure grasp: FALSE')
                return
        distance = map(float, distance)
        self.largest_minimum_resisted_wrench = min(distance)
        self.largest_minimum_resisted_wrench_plane = self.gws.equations[distance.index(self.largest_minimum_resisted_wrench)]
        self.largest_minimum_resisted_wrench_simplex = self.gws.simplices[distance.index(self.largest_minimum_resisted_wrench)]        
        self.is_force_closure_grasp =  True
        self.logger.info('Time taken for FC with minimum resisted wrench is %f seconds', time.clock()-start_time)
        self.logger.info('Force closure grasp: TRUE')
        self.logger.info('Largest minimum resisted wrench is: %f ', self.largest_minimum_resisted_wrench)
        
    def quat_rotate_direction(self, source_dir, target_dir):
        
        def quatFromAxisAngle(axis,angle):
            axis_len = numpy.linalg.norm(axis)
            if(axis_len == 0):
                return numpy.array([1,0,0,0])
            angle = angle*0.5
            sang = numpy.sin(angle)/axis_len
            return numpy.array([numpy.cos(angle), axis[0]*sang, axis[1]*sang, axis[2]*sang])
            
        source_dir = numpy.asarray(source_dir)    
        rot_to_direction = numpy.cross(source_dir,target_dir)
        fsin = numpy.linalg.norm(rot_to_direction)
        fcos = numpy.dot(source_dir,target_dir)
        if (fsin>0):
            return quatFromAxisAngle(rot_to_direction*(1/fsin), numpy.arctan2(fsin,fcos))
        if (fcos<0):
            rot_to_direction = numpy.array([1,0,0])
            rot_to_direction = rot_to_direction - (source_dir * numpy.dot(source_dir,rot_to_direction))
            if (numpy.dot(rot_to_direction,rot_to_direction) < 1e-8):
                rot_to_direction = numpy.array([0,0,1])
                rot_to_direction = rot_to_direction - (source_dir * numpy.dot(source_dir,rot_to_direction))
            rot_to_direction = rot_to_direction/numpy.linalg.norm(rot_to_direction)
            return quatFromAxisAngle(rot_to_direction, numpy.arctan2(fsin,fcos))
        return numpy.array([1,0,0,0])
        
    def rotation_matrix_from_quat(self, quat):
        length_2 = numpy.dot(quat,quat)
        i_length_2 = 2/length_2
        qq1 = i_length_2*quat[1]*quat[1]
        qq2 = i_length_2*quat[2]*quat[2]
        qq3 = i_length_2*quat[3]*quat[3]
        rot = numpy.array([
                [1-qq2-qq3, i_length_2*(quat[1]*quat[2] - quat[0]*quat[3]), i_length_2*(quat[1]*quat[3] + quat[0]*quat[2])],
                [i_length_2*(quat[1]*quat[2] + quat[0]*quat[3]), 1-qq1-qq3, i_length_2*(quat[2]*quat[3] - quat[0]*quat[1])],
                [i_length_2*(quat[1]*quat[3] - quat[0]*quat[2]), i_length_2*(quat[2]*quat[3] + quat[0]*quat[1]), 1-qq1-qq2]
                ])
        return rot
        
    def get_vector_angles(self, v1, v2):

        def dotproduct(v1, v2):
          return sum((a*b) for a, b in zip(v1, v2))

        def length(v):
          return numpy.sqrt(dotproduct(v, v))   
        
        return numpy.arccos(dotproduct(v1, v2) / (length(v1) * length(v2)))    
        
    def create_plots(self):
        self.figure = plt.figure()
        if self.contact_dimension == '3d':
            self.plot_primitive_forces()
            self.plot_primitive_wrenches()
            self.plot_force_space()
            self.plot_torque_space()
        elif self.contact_dimension == '2d':
            self.plot_wrench_space_2d()
        plt.tight_layout()
        plt.show()
        
    def plot_primitive_forces(self):
        primitive_force_plot = self.figure.add_subplot(223, projection='3d')
        primitive_force_plot.set_title("Object Primitive Forces")
        primitive_force_plot.set_xlabel('Fx')
        primitive_force_plot.set_ylabel('Fy')
        primitive_force_plot.set_zlabel('Fz')
        
        colors = cm.rainbow(numpy.linspace(0, 1, len(self.contact_points)))
        
        for i,c in zip(range(len(self.contact_points)),colors):
            for j in range(len(self.contact_wise_object_primitive_wrenches[i])):
                primitive_force_plot.plot([self.contact_points[i][0],self.contact_wise_object_primitive_wrenches[i][j][0]],[self.contact_points[i][1],self.contact_wise_object_primitive_wrenches[i][j][1]],[self.contact_points[i][2],self.contact_wise_object_primitive_wrenches[i][j][2]], color=c)
            sorted_wrenches = self.contact_wise_object_primitive_wrenches[i]
            for j in range(len(sorted_wrenches)):
                if j == len(sorted_wrenches)-1:
                    primitive_force_plot.plot([sorted_wrenches[j][0],sorted_wrenches[0][0]], [sorted_wrenches[j][1],sorted_wrenches[0][1]], [sorted_wrenches[j][2],sorted_wrenches[0][2]], color=c)
                else:
                    primitive_force_plot.plot([sorted_wrenches[j][0],sorted_wrenches[j+1][0]], [sorted_wrenches[j][1],sorted_wrenches[j+1][1]], [sorted_wrenches[j][2],sorted_wrenches[j+1][2]], color=c)
            primitive_force_plot.scatter(self.contact_points[i][0], self.contact_points[i][1], self.contact_points[i][2], color=c, marker='x', s=25)

    def plot_primitive_wrenches(self):
        primitive_torque_plot = self.figure.add_subplot(224, projection='3d')
        primitive_torque_plot.set_title("Object Primitive Torques")
        primitive_torque_plot.set_xlabel('Tx')
        primitive_torque_plot.set_ylabel('Ty')
        primitive_torque_plot.set_zlabel('Tz')
        
        colors = cm.rainbow(numpy.linspace(0, 1, len(self.contact_points)))
        
        for i,c in zip(range(len(self.contact_points)),colors):
            for j in range(len(self.contact_wise_object_primitive_wrenches[i])):
                primitive_torque_plot.plot([self.contact_points[i][0],self.contact_wise_object_primitive_wrenches[i][j][3]],[self.contact_points[i][1],self.contact_wise_object_primitive_wrenches[i][j][4]],[self.contact_points[i][2],self.contact_wise_object_primitive_wrenches[i][j][5]], color=c)
            sorted_wrenches = self.contact_wise_object_primitive_wrenches[i]
            for j in range(len(sorted_wrenches)):
                if j == len(sorted_wrenches)-1:
                    primitive_torque_plot.plot([sorted_wrenches[j][3],sorted_wrenches[0][3]], [sorted_wrenches[j][4],sorted_wrenches[0][4]], [sorted_wrenches[j][5],sorted_wrenches[0][5]], color=c)
                else:
                    primitive_torque_plot.plot([sorted_wrenches[j][3],sorted_wrenches[j+1][3]], [sorted_wrenches[j][4],sorted_wrenches[j+1][4]], [sorted_wrenches[j][5],sorted_wrenches[j+1][5]], color=c)
            primitive_torque_plot.scatter(self.contact_points[i][0], self.contact_points[i][1], self.contact_points[i][2], color=c, marker='x', s=25)

    def plot_force_space(self):
        forces = self.wrenches[:,0:3]
        force_hull= sp_spatial.ConvexHull(forces)
        
        force_plot = self.figure.add_subplot(221, projection='3d')
        force_plot.set_title("Grasp Force Space")
        force_plot.set_xlabel('Fx')
        force_plot.set_ylabel('Fy')
        force_plot.set_zlabel('Fz')
        for simplex in force_hull.simplices:
            xs, ys, zs = forces[simplex].T
            xs = numpy.r_[xs, xs[0]] # close polygons
            ys = numpy.r_[ys, ys[0]]
            zs = numpy.r_[zs, zs[0]]
            force_plot.plot(xs, ys, zs)

    def plot_torque_space(self):
        torques = self.wrenches[:,3:6]
        torque_hull= sp_spatial.ConvexHull(torques)

        torque_plot = self.figure.add_subplot(222, projection='3d')
        torque_plot.set_title("Grasp Torque Space")
        torque_plot.set_xlabel('Tx')
        torque_plot.set_ylabel('Ty')
        torque_plot.set_zlabel('Tz')
        for simplex in torque_hull.simplices:
            xs, ys, zs = torques[simplex].T
            xs = numpy.r_[xs, xs[0]] # close polygons
            ys = numpy.r_[ys, ys[0]]
            zs = numpy.r_[zs, zs[0]]
            torque_plot.plot(xs, ys, zs)
            
    def plot_wrench_space_2d(self):
        gws_plot = self.figure.add_subplot(111, projection='3d')
        gws_plot.set_title("Grasp Wrench Space")
        gws_plot.set_xticks(numpy.arange(-5,5+2,2))
        gws_plot.set_xlim(-5,5)
        gws_plot.set_yticks(numpy.arange(-6,4+2,2))
        gws_plot.set_ylim(-6,4)
        gws_plot.set_zticks(numpy.arange(-50,50+20,20))
        gws_plot.set_zlim(-50,50)
        gws_plot.set_xlabel('Fx')
        gws_plot.set_ylabel('Fy')
        gws_plot.set_zlabel('T')
        gws_plot.scatter(self.wrenches[:,0], self.wrenches[:,1], self.wrenches[:,2],color='black',marker='x',s=15)
        #gws_plot.scatter(0, 0, 0,color='red',marker='o',s=15)
        # Reference: http://math.stackexchange.com/questions/646420/find-the-vector-equation-of-a-line-perpendicular-to-the-plane
        #gws_plot.plot([0, -self.largest_minimum_resisted_wrench_plane[0]*self.largest_minimum_resisted_wrench_plane[-1]],[0, -self.largest_minimum_resisted_wrench_plane[1]*self.largest_minimum_resisted_wrench_plane[-1]],[0, -self.largest_minimum_resisted_wrench_plane[2]*self.largest_minimum_resisted_wrench_plane[-1]], color='red')
        #gws_plot.scatter(-self.largest_minimum_resisted_wrench_plane[0]*self.largest_minimum_resisted_wrench_plane[-1], -self.largest_minimum_resisted_wrench_plane[1]*self.largest_minimum_resisted_wrench_plane[-1], -self.largest_minimum_resisted_wrench_plane[2]*self.largest_minimum_resisted_wrench_plane[-1],color='red',marker='o',s=15)
        for simplex in self.gws.simplices:
            # Reference: http://stackoverflow.com/questions/4622057/plotting-3d-polygons-in-python-matplotlib
            vertices = self.wrenches[simplex]
            tri = mplot3d.art3d.Poly3DCollection([vertices])
            tri.set_color('blue')
            tri.set_edgecolor('k')
            tri.set_alpha(0.2)
            if (simplex == self.largest_minimum_resisted_wrench_simplex).all():
                tri.set_alpha(0.5)
            gws_plot.add_collection3d(tri)
